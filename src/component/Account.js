import React from 'react'
import { Container } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";

export default function Account() {
    return (
        <Router>
            <Container>
                <h2>Accounts</h2>

                <ul>
                <li>
                    <Link to="/netflix">Netflix</Link>
                </li>
                <li>
                    <Link to="/zillow-group">Zillow Group</Link>
                </li>
                <li>
                    <Link to="/yahoo">Yahoo</Link>
                </li>
                <li>
                    <Link to="/modus-create">Modus Create</Link>
                </li>
                </ul>

                <Switch>
                <Route path="/:id" children={<Child />} />
                </Switch>
            </Container>
            </Router>
    );
}
function Child() {
    // We can use the `useParams` hook here to access
    // the dynamic pieces of the URL.
    let { id } = useParams();
    const hStyle = { color: 'red' };

    return (
      <div>
        <h3>ID: <a style={ hStyle }>{id}</a></h3>
      </div>
    );
  }
  
