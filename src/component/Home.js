
import React from 'react'
import {Link} from 'react-router-dom'
import {Table,Button,Card} from 'react-bootstrap'

function Home({data}) {
    console.log(data);


    return (
        <div className="container">
            <h2>List Data</h2>
            {/* <Link to={`/View/5`}>View</Link>
            <Link to={`/ViewPath?name=dara&&sex=M`}>ViewPath</Link> */}
        

{/* Create Cards for products */}

{/* 
        <div className="row">
                 <div className="col-md-3"> 
                    
                 {data.map((data)=>
                    <Card>
                        <Card.Img variant="top" src={data.image} style={{width:'50px'}} />
                        <Card.Body>
                            <Card.Title>HTML</Card.Title>
                            <Card.Text>
                            {data.description}
                            </Card.Text>
                            <Link to={`/View/${data.id}`}><Button variant="primary" size="sm">View</Button></Link>
                        </Card.Body>
                    
                    </Card>)}
                 </div>
            </div>
<br></br> */}
        <Table striped bordered hover>
           
            <tbody>
                {data.map((data)=><tr>
                <td>{data.id}</td>
                <td>{data.title}</td>
                <td>{data.description}</td>
                <td><img src={data.image} style={{width:'50px'}} /></td>
                <td><Link to={`/View/${data.id}`}><Button variant="primary" size="sm">View</Button></Link></td>
                </tr>)}
            </tbody>
            </Table>

        </div>
    )
}

export default Home
