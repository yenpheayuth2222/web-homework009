import React from 'react'
import { Container, ButtonGroup } from 'react-bootstrap';
import {BrowserRouter as Router,Switch,Route,Link,useParams,useRouteMatch} from "react-router-dom";

export default function Video() {
    return (
        <Router>
      <Container>
      <h2>Video</h2>
        <ButtonGroup aria-label="Basic example">
        <Link to="/Movie" size="sm" className="btn btn-info">Movie</Link>
        <Link to="/Animation"  size="btn-sm" className="btn btn-info">Animation</Link>
        </ButtonGroup>

        <Switch>
          <Route exact path="/Movie">
            <Movie />
          </Route>
          <Route path="/Animation">
            <Animation />
          </Route>
        </Switch>
      </Container>
    </Router>
    );
}
  
function Movie(){
    let { path, url } = useRouteMatch();
  
    return (
      <div>
        <h2>Animation Category</h2>
        <ButtonGroup aria-label="Basic example">
            <Link to={`${url}/Action`} size="sm" className="btn btn-info">Action</Link>
            <Link to={`${url}/Romance`} size="sm" className="btn btn-info">Romance</Link>
            <Link to={`${url}/Comedy`} size="sm" className="btn btn-info">Comedy</Link>
                      
        </ButtonGroup>
       
        <Switch>
          <Route exact path={path}>
            {/* <h3>Please select a topic. </h3> */}
          </Route>
          <Route path={`${path}/:AnimationId`}>
            <Animate />
          </Route>
        </Switch>
      </div>
    );
}

// Create Animation List
  function Animation() {
    let { path, url } = useRouteMatch();
  
    return (
      <div>
        <h2>Animation Category</h2>
        <ButtonGroup aria-label="Basic example">
            <Link to={`${url}/Action`} size="sm" className="btn btn-info">Action</Link>
            <Link to={`${url}/Romance`} size="sm" className="btn btn-info">Romance</Link>
            <Link to={`${url}/Comedy`} size="sm" className="btn btn-info">Comedy</Link>
                      
        </ButtonGroup>
       
        <Switch>
          <Route exact path={path}>
            <h3>Please select a topic. </h3>
          </Route>
          <Route path={`${path}/:AnimationId`}>
            <Animate />
          </Route>
        </Switch>
      </div>
    );
  }

 
//   Animation Id
  function Animate() {
    let { AnimationId } = useParams();
    const hStyle = { color: 'red' };
    return (
      <div>
        <h3>Please select a topic : <a style={ hStyle }>{AnimationId}</a></h3>
      </div>
    );
  }

  

