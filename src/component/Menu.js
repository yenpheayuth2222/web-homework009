import React from 'react'
import { Button, Nav, Form, Navbar, FormControl } from 'react-bootstrap';
import {Link} from 'react-router-dom'

function Menu() {
    return (
        <div>
            
            <Navbar bg="light" expand="lg">
            <div className="container">
                <Navbar.Brand href="#">React-Router</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                    className="mr-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                    >
                    <Nav.Link as={Link} to="/">Home</Nav.Link>
                    {/* <Nav.Link as={Link} to="/View">View</Nav.Link> */}
                    <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                    <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                    <Nav.Link as={Link} to="/Welcome">Welcome</Nav.Link>
                    <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
                    </Nav>
                    <Form className="d-flex">
                    <FormControl
                        type="search"
                        placeholder="Search"
                        className="mr-2"
                        aria-label="Search"
                    />
                    <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
                </div> 
                </Navbar>
               
        </div>
    )
}

export default Menu
