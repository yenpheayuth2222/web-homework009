import React from 'react'
import { Container,Button } from 'react-bootstrap'
import {Redirect} from 'react-router-dom'

function Welcome({authorized}) {
    if (!authorized) {
        return <Redirect to="/Auth" />
    }
    return (
        <Container>
            <h2>Welcome</h2>
            <Button>Logout</Button>
        </Container>
    )
}

export default Welcome
