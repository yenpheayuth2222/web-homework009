import React from 'react'
import {Modal} from 'react-bootstrap'

function View({match, data}) {

    console.log(data);
    console.log(match.params.id);
    var dataView = data.find((data)=> data.id == match.params.id)
    console.log(dataView);

    return (
        <div className="container">

            <Modal.Dialog>
            <Modal.Header closeButton>
                <Modal.Title>Detail : <a className="text-danger">{dataView.id}</a></Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <h1 className="text-center">{dataView.title}</h1>
                <img src={dataView.image} style={{width:"450px", textAlign:"center"}}/>
                <h4>Description : </h4>
                <p>{dataView.description} </p>
            </Modal.Body>

            </Modal.Dialog>      
        </div>
    )
}

export default View
    