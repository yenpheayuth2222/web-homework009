import Menu from './component/Menu'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import Home from './component/Home'
import Account from './component/Account'
import Video from './component/Video'
import Welcome from './component/Welcome'
import Auth from './component/Auth'
import View from './component/View'
import {Container} from 'react-bootstrap'
import React, { Component } from 'react'

export default class App extends Component {
  
  constructor(){
    super();
    this.state={
      data:[{
        id: 1,
        title: 'HTML',
        description: 'HTML stands for Hyper Text Markup Language· HTML is the standard markup language for creating Web pages ·',
        image: 'https://cdn.educba.com/academy/wp-content/uploads/2018/09/Top-Uses-Of-HTML.jpg'
      },
      {
        id: 2,
        title: 'CSS',
        description: 'Stands for "Cascading Style Sheet." Cascading style sheets are used to format the layout of Web pages. ',
        image: 'https://sabe.io/classes/css/hero.png'
      },
      {
        id: 3,
        title: 'JavaScript',
        description: 'JavaScript (often shortened to JS) is a lightweight, interpreted, object-oriented language',
        image: 'https://www.doowebs.eu/wp-content/uploads/2016/08/javascript-1.png'
      },
      {
        id: 4,
        title: 'Bootstrap 4',
        description: 'Bootstrap 4 is popular mobile first front-end framework for building responsive mobile first sites on the web.',
        image: 'https://eyfriend.co.za/wp-content/uploads/2021/02/bootstrap-stack.png'
      },
      {
        id: 5,
        title: 'ReactJS',
        description: 'ReactJS is JavaScript library used for building reusable UI components.',
        image: 'https://cdn.educba.com/academy/wp-content/uploads/2018/10/Top-Uses-Of-reactjs.jpg'
      },
      {
        id: 6,
        title: 'Java',
        description: 'Java is a general-purpose, class-based, object-oriented programming language dependencies.',
        image: 'https://miro.medium.com/max/8642/1*iIXOmGDzrtTJmdwbn7cGMw.png'
      },
      {
        id: 7,
        title: 'Spring Boot',
        description: 'Spring Boot is an open source Java-based framework used to create a micro Service. ',
        image: 'https://miro.medium.com/max/850/1*WsfmenxQNE1hhZasxsIGhA.png'
      },
      {
        id: 8,
        title: 'Docker',
        description: 'Docker is an open platform for developing, shipping, and running applications. ',
        image: 'https://kostacipo.stream/wp-content/uploads/2021/02/docker-container.jpg'
      }]
    }
  }

  render() {
    return (
      <div>
        <Router>
        <Menu/>
        <Switch>
          <Route path='/' exact render={()=> <Home data={this.state.data}/>} />
          <Route path='/View/:id' render={(props)=> <View {...props} data={this.state.data}/>} />
          <Route path='/Video' component={Video} />
          <Route path='/Account' component={Account} />
          <Route path='/Welcome' component={()=> <Welcome authorized={true} />} />
          <Route path='/Auth' component={Auth} />
          <Route path='*' render={() => <Container><h1>404 Page Not Found</h1></Container>} />
        </Switch>
      </Router>
      </div>
    )
  }
}
